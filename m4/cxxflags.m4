AC_DEFUN([__AX_INIT_CXXFLAGS], [
	AC_SUBST([EXTRA_CXXFLAGS])
])

AC_DEFUN([__AX_CXXFLAG_SHQUOTE], [[ax_cv_cppflag_]AS_TR_SH([$1])])

AC_DEFUN([__AX_CHECK_CXXFLAG], [
	AC_REQUIRE([__AX_INIT_CXXFLAGS])
	AC_LANG([C++])
	CXXFLAGS_bak="$CXXFLAGS"
	CXXFLAGS="$CXXFLAGS_bak $1"
	AC_COMPILE_IFELSE([
		AC_LANG_PROGRAM([int a;], [a = 0; return a;])
	], [
		__AX_CXXFLAG_SHQUOTE([$1])=yes
	], [
		__AX_CXXFLAG_SHQUOTE([$1])=no
	])
	CXXFLAGS="$CXXFLAGS_bak"
])

AC_DEFUN([AX_CHECK_CXXFLAG], [
	AC_CACHE_CHECK([whether ]$CXX[ accepts $1], __AX_CXXFLAG_SHQUOTE([$1]), [__AX_CHECK_CXXFLAG([$1])])
	AS_IF(test $__AX_CXXFLAG_SHQUOTE([$1]) = no, [$2], [EXTRA_CXXFLAGS="$EXTRA_CXXFLAGS $1"])
])

AC_DEFUN([AX_CHECK_CXXFLAGS], [
	__AX_INIT_CXXFLAGS
	AX_CHECK_CXXFLAG([-std=gnu++1z], [
		AX_CHECK_CXXFLAG([-std=c++1z], [
			AX_CHECK_CXXFLAG([-std=gnu++14], [
				AX_CHECK_CXXFLAG([-std=c++14], [
					AX_CHECK_CXXFLAG([-std=gnu++11], [
						AX_CHECK_CXXFLAG([-std=c++11], [
							AX_CHECK_CXXFLAG([-std=gnu++03], [
								AX_CHECK_CXXFLAG([-std=c++03])
							])
						])
					])
				])
			])
		])
	])
	AX_CHECK_CXXFLAG([-frtti])
  AX_CHECK_CXXFLAG([-fabi-version=10], [
    AX_CHECK_CXXFLAG([-fabi-version=9], [
      AX_CHECK_CXXFLAG([-fabi-version=8], [
        AX_CHECK_CXXFLAG([-fabi-version=7], [
          AX_CHECK_CXXFLAG([-fabi-version=6], [
            AX_CHECK_CXXFLAG([-fabi-version=5], [
              AX_CHECK_CXXFLAG([-fabi-version=4], [
                AX_CHECK_CXXFLAG([-fabi-version=3], [
                  AX_CHECK_CXXFLAG([-fabi-version=2], [
                    AX_CHECK_CXXFLAG([-fabi-version=1], [
                      AX_CHECK_CXXFLAG([-fabi-version=0])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
  AX_CHECK_CXXFLAG([-fabi-compat-version=10], [
    AX_CHECK_CXXFLAG([-fabi-compat-version=9], [
      AX_CHECK_CXXFLAG([-fabi-compat-version=8], [
        AX_CHECK_CXXFLAG([-fabi-compat-version=7], [
          AX_CHECK_CXXFLAG([-fabi-compat-version=6], [
            AX_CHECK_CXXFLAG([-fabi-compat-version=5], [
              AX_CHECK_CXXFLAG([-fabi-compat-version=4], [
                AX_CHECK_CXXFLAG([-fabi-compat-version=3], [
                  AX_CHECK_CXXFLAG([-fabi-compat-version=2], [
                    AX_CHECK_CXXFLAG([-fabi-compat-version=1], [
                      AX_CHECK_CXXFLAG([-fabi-compat-version=0])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
	AX_CHECK_CXXFLAG([-faccess-control])
	AX_CHECK_CXXFLAG([-felide-constructors])
	AX_CHECK_CXXFLAG([-fext-numeric-literals])
	AX_CHECK_CXXFLAG([-fstrict-enums])
	AX_CHECK_CXXFLAG([-funsigned-char])
	AX_CHECK_CXXFLAG([-fheinous-gnu-extensions])
])
