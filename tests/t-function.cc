#include <atl/function>
#include <cstring>
#include <string>

int
fn1 (const char *buf)
{
  return strlen (buf);
}

char
fn2 (int x)
{
  return (char) ('0' + (x % 10));
}

std::string
fn3 (char ch)
{
  return std::string{(std::size_t) ch, ch};
}

int
main (void)
{
  atl::function<const char *, int> f1 = fn1;
  atl::function<int, char> f2 = fn2;
  atl::function<char, std::string> f3 = fn3;
  atl::function<const char *, char> f12 = f1 + f2;
  atl::function<int, std::string> f23 = f2 + f3;
  atl::function <const char *, std::string> f123 = f1 + f2 + f3;


  return 0;
}
