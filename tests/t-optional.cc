#include <atl/optional>
#include <cstdlib>
#include <iostream>

using opt = atl::optional<int>;
using namespace std;

void
__assert (const char *file, size_t line, const char *expr, bool expr_succ)
{
  if (!expr_succ)
    {
      clog << file << ":" << line << ": " << "Assertion failed: " << expr
        << endl;
      exit (1);
    }
  else
    clog << file << ":" << line << ": " << "Assertion successful: " << expr
      << endl;
}

#define assert(expr) __assert ((__FILE__), (__LINE__), (#expr), !!(expr))

int
main (void)
{
  try
    {
      // constructor
      opt o1, o2{1}, o3{2}, o4{3};

      // operator bool(), operator !()
      assert (!o1);
      assert (o2);
      assert (o3);
      assert (o4);

      o2 = o3;

      assert (o2);
      assert (o3);

      assert (o2 == o3);

      o2 = o1;

      assert (!o2);
      assert (!o1);

      assert (o2 == o1);

      // (implicit) destructor
    }
  catch (...)
    {
      // Uncaught exception
      return 1;
    }
  return 0;
}
