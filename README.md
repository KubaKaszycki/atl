# Additional Template Library
**ATL** is a C++ library which provides useful templates for everyday
development, so, if a particular template is not in STL, and it is in ATL, it
may save you much time if include ATL in your project's dependencies. It is
licensed under 2-clause FreeBSD license, so, if you want to redistribute code
or documentation, make sure you don't remove the license, and you can.
