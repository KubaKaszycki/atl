// -*- C++ -*-

/*-
 * Copyright (c) 2016, Jakub Kaszycki
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LIBATL___OPTIONAL_IMPL_H
#define LIBATL___OPTIONAL_IMPL_H

// ==== Placed in an external header to avoid mess ====

// If someone includes this file solely, tell them to change their program
#ifndef LIBATL_OPTIONAL
# error Do not include <atl/__optional_impl.h> directly! Use <atl/optional>.
#endif

// But still if someone ignores #error, include API header
// No recursivity will happen because of header guards
#include <atl/optional>

template <class _T>
inline
atl::optional<_T>::optional () noexcept : _ptr (nullptr)
{
}

template <class _T>
inline
atl::optional<_T>::optional (const atl::optional<_T>::optional_type& other)
  noexcept : _ptr (other._ptr)
{
}

template <class _T>
inline
atl::optional<_T>::optional (atl::optional<_T>::ref_type ref) noexcept
  : _ptr (&ref)
{
}

template <class _T>
inline
atl::optional<_T>::optional (atl::optional<_T>::ptr_type ptr) noexcept
  : _ptr (ptr)
{
}

template <class _T>
inline
atl::optional<_T>::~optional () noexcept
{
}

template <class _T>
inline bool
atl::optional<_T>::operator== (const atl::optional<_T>::optional_type& other)
  const noexcept
{
  return _ptr == other._ptr;
}

template <class _T>
inline bool
atl::optional<_T>::operator!= (const atl::optional<_T>::optional_type& other)
  const noexcept
{
  return _ptr != other._ptr;
}

template <class _T>
inline typename atl::optional<_T>::optional_type &
atl::optional<_T>::operator= (const atl::optional<_T>::optional_type& other)
  noexcept
{
  _ptr = other._ptr;
  return *this;
}

template <class _T>
inline void
atl::optional<_T>::set (atl::optional<_T>::ref_type ref) noexcept
{
  _ptr = &ref;
}

template <class _T>
inline void
atl::optional<_T>::unset (void) noexcept
{
  _ptr = nullptr;
}

template <class _T>
inline bool
atl::optional<_T>::is (void) const noexcept
{
  return _ptr != nullptr;
}

template <class _T>
inline
atl::optional<_T>::operator bool (void) const noexcept
{
  return is ();
}

template <class _T>
inline bool
atl::optional<_T>::operator! (void) const noexcept
{
  return !is ();
}

template <class _T>
inline typename atl::optional<_T>::ref_type
atl::optional<_T>::value (void) const throw (std::logic_error)
{
  if (_ptr == nullptr)
    throw std::logic_error{std::string{__func__} + ": Attempting to get value "
        "from empty optional"};
  return *_ptr;
}

template <class _T>
inline
atl::optional<_T>::operator ref_type (void) const throw (std::logic_error)
{
  return value ();
}

template <class _T>
inline
atl::optional<_T>::operator ptr_type (void) const throw (std::logic_error)
{
  return &value ();
}

#endif /* LIBATL___OPTIONAL_IMPL_H */
