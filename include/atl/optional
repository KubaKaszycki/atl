// -*- C++ -*-

/*-
 * Copyright (c) 2016, Jakub Kaszycki
 * 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LIBATL_OPTIONAL
#define LIBATL_OPTIONAL

#include <stdexcept>

namespace atl
{
  template <class _T>
  class optional
  {
  public:
    using value_type = _T;
    using ref_type = const value_type &;
    using optional_type = optional<value_type>;
    using ptr_type = const value_type *;

    optional () noexcept;
    optional (const optional_type&) noexcept;
    optional (ref_type) noexcept;
    optional (ptr_type) noexcept;
    virtual ~optional () noexcept;

    virtual bool operator== (const optional_type&) const noexcept;
    virtual bool operator!= (const optional_type&) const noexcept;
    virtual optional_type& operator= (const optional_type&) noexcept;

    virtual void set (ref_type) noexcept;
    virtual void unset (void) noexcept;

    virtual bool is (void) const noexcept;
    virtual operator bool (void) const noexcept;
    virtual bool operator! (void) const noexcept;

    virtual ref_type value (void) const throw (std::logic_error);
    virtual operator ref_type (void) const throw (std::logic_error);
    virtual operator ptr_type (void) const throw (std::logic_error);

  private:
    ptr_type _ptr;
  };
}

#include <atl/__optional_impl.h>

#endif /* LIBATL_OPTIONAL */
